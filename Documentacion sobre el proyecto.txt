Esta es la documentacion respecto al proyecto. La idea es crear una pagina web que permita al usuario registrarse con Facebook y que pueda
realizar dentro de la misma  una serie de tareas por lote usando la api de Facebook.

Como primer paso, el usuario deberia loguearse y conceder los permisos necesarios para realizar dichas tareas.

- Las mismas consisten en:
      
      * Una funcion que te permita descargar todas tus fotos, tanto las etiquetadas como las propias (si se puede, que las descargue en carpetas)
      * Una funcion que te permita eliminar el "Me Gusta" a todas las paginas a las que un usuario les dio "Me Gusta" en la antiguedad.
      * Una funcion que permita al usuario eliminar un conjunto grande (o todas) de conversaciones; posiblemente clasificadas por fecha.
      * Una funcion que permita al usuario seleccionar muchos amigos de su lista y eliminarlos haciendo solo 1 click.


El repositorio cuenta con dos carpetas principales; las cuales se dividen en los archivos correspondientes de la pagina web propiamente
dicha y los archivos correspondientes al servidor web que las soporta.

Ademas, en la raiz del repositorio, existe un archivo de Tareas Realizadas y Tareas Por Hacer; donde se iria anotando qué cosas faltan
por hacer; aquellos inconvenientes que quedaron sin resolver o no se pudieron; y aquellas cuestiones que van ocurriendo o vamos creyendo
conveniente anotar y recordar para la posterioridad.
Asi como tambien, pasar las tareas anotadas y ya realizadas a la seccion de tareas realizadas; a modo de llevar un historial de lo que vamos
haciendo en la pagina.

